# from apyori import apriori
import json

# install orange using:
# ./pip install orange3
# ./pip install orange3-associate

from orangecontrib.associate.fpgrowth import *


product_ids_with_metadata = {}

transactions = []

print('Reading file')
for line in open('FilteredAmazonDatasetWithDifferentCategories.json', 'r'):
    metadata = json.loads(line)
    product_ids_with_metadata[metadata['asin']] = metadata

    root_product_id = metadata['asin']
    if 'related' in metadata.keys():
        if 'also_bought' in metadata['related'].keys():
            local_transactions = []
            local_transactions.append(root_product_id)

            # get product ids that are bought together with root product id
            bought_together_product_ids = metadata['related']['also_bought']

            # loop over bought_together_product_ids and add ids into local_transactions
            for product_id in bought_together_product_ids:
                local_transactions.append(product_id)
            # finally add local transactions into transactions
            transactions.append(local_transactions)

print('Done reading file and constructing transactions table')

print('Applying Apriori algorithm from Orange library')
itemsets = frequent_itemsets(transactions, 1)
# print(len(list(itemsets)))
# print('Done')
# exit()

# itemset holds automatically recommended products that match each other

# loop over itemset to get automatically recommended products
for item in itemsets:

    batch_of_recommended_products = list(item[0])
    line = open('MatchedTogetherItems.txt', 'a')
    line.write('\n')
    line.write('----------The following product ids are recommended by Apriori--------------')
    line.write('\n')
    for product_id in batch_of_recommended_products:
        if product_id in product_ids_with_metadata:
            line.write(product_id + ' - ')
            line.write(product_ids_with_metadata[product_id]['title'])
            line.write('\n')
