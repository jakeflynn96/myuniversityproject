$(function() {
    // Reads the json also known as the dataset
  $.getJSON('ClothesDatabase.json', function(data) {
    // loops over it
    for (var i = 0; i < data.products.length; i++) {
      var obj = data.products[i];
      // If the title contains 'shorts'
      if (obj.title.toLowerCase().includes("shorts")) {
        // Then append that pants image to the html
        var Imgs = '<div class="Pants"><img alt="image of pants" src="' + obj.imUrl + '"></div>'
        $(Imgs).appendTo($(".PantsImages"));
      }
    }
})})

var PantIndex = 1;
showPants(PantIndex);

// Next/previous controls
function plusPants(n) {
  showPants(PantIndex += n);
}

// Thumbnail image controls
function currentPant(n) {
  showPants(PantIndex = n);
}

function showPants(n) {
  var i;
    // get class name
  var Pants = document.getElementsByClassName("Pants");
  if (n > Pants.length) {PantIndex = 1}
    // if it amount of pants is less than one make the pantsindex equal to the pants length
  if (n < 1) {PantIndex = Pants.length}
    // loop over the pants
  for (i = 0; i < Pants.length; i++) {
      // display them all as none
      Pants[i].style.display = "none";
  }
    // display the first pants image
  Pants[PantIndex-1].style.display = "block";
}
