$(function() {
  // Reads the json also known as the dataset
    $.getJSON('ClothesDatabase.json', function(data) {
      // loops over it
      for (var i = 0; i < data.products.length; i++) {
        var obj = data.products[i];
        // If the title contains 'Hat'
        if (obj.title.toLowerCase().includes("hat")) {
          // Then append that hat image to the html
          var Imgs = '<div class="Hats"><img alt="image of a hat" src="' + obj.imUrl + '"></div>'
          $(Imgs).appendTo($(".HatsImages"));
        }
      }
})})

var HatIndex = 1;
showHats(HatIndex);

// Next/previous controls
function plusHats(n) {
  showHats(HatIndex += n);
}

// Thumbnail image controls
function currentHat(n) {
  showHats(HatIndex = n);
}

function showHats(n) {
  var i;
  // get class name
  var Hats = document.getElementsByClassName("Hats");
  if (n > Hats.length) {HatIndex = 1}
  // if it amount of hats is less than one make the hatindex equal to the hat length
  if (n < 1) {HatIndex = Hats.length}
  // loop over the hats
  for (i = 0; i < Hats.length; i++) {
    // display them all as none
      Hats[i].style.display = "none";
  }
  // display the first hat image
  Hats[HatIndex-1].style.display = "block";
}
