$(function() {
    // Reads the json also known as the dataset
  $.getJSON('ClothesDatabase.json', function(data) {
    // loops over it
    for (var i = 0; i < data.products.length; i++) {
      var obj = data.products[i];
      // If the title contains 'shoes'
      if (obj.title.toLowerCase().includes("shoes")) {
        // Then append that shoes image to the html
        var Imgs = '<div class="Shoes"><img alt="image of shoes" src="' + obj.imUrl + '"></div>'
        $(Imgs).appendTo($(".ShoesImages"));
      }
    }
})})

var ShoeIndex = 1;
showShoes(ShoeIndex);

// Next/previous controls
function plusShoes(n) {
  showShoes(ShoeIndex += n);
}

// Thumbnail image controls
function currentShoe(n) {
  showShoes(ShoeIndex = n);
}

function showShoes(n) {
  var i;
    // get class name
  var Shoes = document.getElementsByClassName("Shoes");
  if (n > Shoes.length) {ShoeIndex = 1}
    // if it amount of shoes is less than one make the shoesindex equal to the shoes length
  if (n < 1) {ShoeIndex = Shoes.length}
  // loop over the shoes
  for (i = 0; i < Shoes.length; i++) {
    // display them all as none
      Shoes[i].style.display = "none";
  }
    // display the first shoes image
  Shoes[ShoeIndex-1].style.display = "block";
}
