$(document).ready(function(){
    //when the hamburger menu is clicked
    $('#navbar-icon').click(function(){
        //and the menu is not displayed
        if ($('#menu').css('display') === 'none') {
            //then add the class open to the element
            $(this).toggleClass('open');
            //slide the menu down
            $('#menu').slideDown();
            //give it some extra styling
            $(this).css('border-color', 'white');
            $(this).css('background-color', 'white');
            $('body').css('overflow','hidden');
            $('#menu').css('display', 'block');
        }else{
            //if not then set body overflow to auto
            $('body').css('overflow','auto');
            //give it a class called open
            $(this).toggleClass('open');
            //slide the menu up
            $('#menu').slideUp();
            //give it some extra styling
            $(this).css('border-color', 'black');
            $(this).css('background-color', 'black');
            $(".navbar-icon > span").css('background-color', '#0072CA');
        }
    });

    //if a link in the hamburger menu is clicked
    $('#menu > div > a').click(function(){
        //set body overflow to auto
            $('body').css('overflow','auto');
            //set it class as open
            $('#navbar-icon').toggleClass('open');
            //slide the menu up
            $('#menu').slideUp();
            //and give it some extra needed stylings
            $('#navbar-icon').css('border-color', 'black');
            $('#navbar-icon').css('background-color', 'black');
            $("#navbar-icon > span").css('background-color', '#0072CA');
    });

});
