//array of words declared as 'sales'
var sales = [
    "SANDALS & FLIPFLOPS SHOW NOW!",
    "JFX UNLIMITED DELIVERY FOR JUST £9.99 A YEAR",
    "NIKE AIR MAX 270 SHOW NOW!",
];

//pull the span tag through from the html file named 'sales'
var getSpanTag = document.getElementById("sales");

//function called changeword
function changeWord() {
    getSpanTag.innerHTML = sales[0]; // push the first key in the span tag
    sales.push(sales.shift());    // then Push the first key to the end of list.
}
changeWord();                  // First run of the function
setInterval(changeWord, 3000); // loop over the function at 1500ms
