$(function() {
      // Reads the json also known as the dataset
  $.getJSON('ClothesDatabase.json', function(data) {
    for (var i = 0; i < data.products.length; i++) {
          // loops over it
      var obj = data.products[i];
        // If the title contains 'shirt'
      if (obj.title.toLowerCase().includes("shirt")) {
        // Then append that tops image to the html
        var Imgs = '<div class="Tops"><img alt="image of a t-shirt" src="' + obj.imUrl + '"></div>'
        $(Imgs).appendTo($(".TopsImages"));
      }
    }
})})

var TopIndex = 1;
showTops(TopIndex);

// Next/previous controls
function plusTops(n) {
  showTops(TopIndex += n);
}

// Thumbnail image controls
function currentTop(n) {
  showTops(TopIndex = n);
}

function showTops(n) {
  var i;
    // get class name
  var Tops = document.getElementsByClassName("Tops");
  if (n > Tops.length) {TopIndex = 1}
      // if it amount of tops is less than one make the topsindex equal to the tops length
  if (n < 1) {TopIndex = Tops.length}
      // loop over the tops
  for (i = 0; i < Tops.length; i++) {
      // display them all as none
      Tops[i].style.display = "none";
  }
  // display the first tops image
  Tops[TopIndex-1].style.display = "block";
}
